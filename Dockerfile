FROM buildroot/base:20200814.2228
RUN git clone git://git.buildroot.net/buildroot
ENV BR2_ROOTFS_OVERLAY=overlay
COPY overlay .
RUN cd buildroot && make BR2_ROOTFS_OVERLAY=$BR2_ROOTFS_OVERLAY raspberrypi3_defconfig
RUN cd buildroot && make source
RUN cd buildroot && make BR2_ROOTFS_OVERLAY=$BR2_ROOTFS_OVERLAY -j8
